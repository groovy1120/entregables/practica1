public class ReduxPersona {
	private String nombre;
    private int edad;
    
    public ReduxPersona() {}
    
    public ReduxPersona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;  
    }
    
    public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public static void main (String[] args){
        ReduxPersona personajesGOT = new ReduxPersona();
		personajesGOT = new ReduxPersona("Jhon Nieve", 20);
		System.out.println("Nombre: " + personajesGOT.getNombre()+" "+ "Edad: " + personajesGOT.getEdad());
    }
}
